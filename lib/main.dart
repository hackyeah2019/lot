import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux_dev_tools/redux_dev_tools.dart';

import 'package:Lot/redux/actions/User.dart';
import 'package:Lot/utils/LotPallete.dart';
import 'package:Lot/redux/AppState.dart';
import 'package:Lot/Login/Login.dart';
import 'package:Lot/Home/Home.dart';
import 'package:Lot/redux/reducers/index.dart';

void main() {
  runApp(new MainApp());
}

class MainApp extends StatelessWidget {
  final store =
      DevToolsStore<AppState>(appStateReducers, initialState: AppState.empty);

  @override
  Widget build(BuildContext context) {
    store.dispatch(GetUserAction());
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: LotPallete.mainBlue, // Color for Android
        statusBarBrightness:
            Brightness.dark // Dark == white status bar -- for IOS.
        ));
    return new StoreProvider<AppState>(
        store: store,
        child: MaterialApp(
          title: 'Lot',
          theme: new ThemeData(
            primarySwatch: LotPallete.mainBlue,
          ),
          home: StoreBuilder<AppState>(
            onInit: (store) => store.dispatch(GetUserAction()),
            builder: (context, store) => store.state.user.logged
                ? HomePageView(store)
                : LoginScreen(store),
          ),
        ));
  }
}
