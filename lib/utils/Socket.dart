import 'dart:convert';
import 'dart:math';

import 'package:web_socket_channel/web_socket_channel.dart';
import 'package:web_socket_channel/io.dart';
import 'dart:async';

class SocketConnection {
  WebSocketChannel channel;
  bool isConnected = false;
  static Random random = new Random();
  String number = random.nextInt(100000).toString();

  WebSocketChannel getChanel() {
    return channel;
  }

  messageListening(data, event, callback) {
    String message = data;
    if (message != '3' && message.substring(0, 2) == '42') {
      var parsedJson = json.decode(message.substring(2));

      if (parsedJson[0] == event) {
        var data = json.decode(parsedJson[1]);
        var decoded = data["data"] as Map<String, dynamic>;

        callback(decoded["type"], decoded["status"], decoded["text"]);
      }
    }
  }

  void emit(String event, String message) {
    channel.sink.add('42["' +
        event +
        '", {"msg": "' +
        message +
        '", "uid": ' +
        number +
        '}]');
  }

  void on(String event, callback) {
    channel.stream.listen((data) => messageListening(data, event, callback));
  }

  void disconnect() {
    channel.sink.close();
    isConnected = false;
  }

  void connection() {
    channel = IOWebSocketChannel.connect(
        'wss://lkononov.herokuapp.com/socket.io/?EIO=3&transport=websocket');

    channel.sink.add('42["authorised", {"uid": ' + number + '}]');
    isConnected = true;
    Timer.periodic(new Duration(seconds: 1), (timer) {
      if (isConnected) {
        channel.sink.add("2probe");
      }
    });
  }
}
