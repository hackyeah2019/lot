import 'package:flutter/material.dart';

class LotPallete {
  static const MaterialColor mainBlue = MaterialColor(0xFF093479, <int, Color>{
    50: Color.fromRGBO(9, 52, 121, .05),
    100: Color.fromRGBO(9, 52, 121, .1),
    200: Color.fromRGBO(9, 52, 121, .2),
    300: Color.fromRGBO(9, 52, 121, .3),
    400: Color.fromRGBO(9, 52, 121, .4),
    500: Color.fromRGBO(9, 52, 121, .5),
    600: Color.fromRGBO(9, 52, 121, .6),
    700: Color.fromRGBO(9, 52, 121, .7),
    800: Color.fromRGBO(9, 52, 121, .8),
    900: Color.fromRGBO(136, 14, 79, 9)
  });
  static const Color backgroundGray = MaterialColor(0xFFf1f2f4, <int, Color>{
    50: Color.fromRGBO(241, 242, 244, .05),
    100: Color.fromRGBO(241, 242, 244, .1),
    200: Color.fromRGBO(241, 242, 244, .2),
    300: Color.fromRGBO(241, 242, 244, .3),
    400: Color.fromRGBO(241, 242, 244, .4),
    500: Color.fromRGBO(241, 242, 244, .5),
    600: Color.fromRGBO(241, 242, 244, .6),
    700: Color.fromRGBO(241, 242, 244, .7),
    800: Color.fromRGBO(241, 242, 244, .8),
    900: Color.fromRGBO(241, 242, 244, 9)
  });
}
