class Message {
  String type;
  String status;
  String text;

  Message({this.type, this.status, this.text});

  factory Message.fromJson(Map<String, dynamic> json) {
    return Message(
        type: json["type"], status: json["status"], text: json["text"]);
  }
}
