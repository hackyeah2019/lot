import 'dart:convert';
import 'package:Lot/redux/actions/Query.dart';
import 'package:Lot/redux/models/Query.dart';
import 'package:Lot/utils/LotPallete.dart';
import 'package:Lot/webview/WebView.dart';
import 'package:flutter/material.dart';
import 'package:Lot/utils/Socket.dart';
import 'package:Lot/redux/AppState.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux_dev_tools/redux_dev_tools.dart';

class ChatBot extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreBuilder<AppState>(
      builder: (context, store) => ChatBotState(store),
    );
  }
}

class ChatBotState extends StatefulWidget {
  final DevToolsStore<AppState> store;

  ChatBotState(this.store);

  _ChatBotView createState() => _ChatBotView(store);
}

class _ChatBotView extends State<ChatBotState> {
  SocketConnection socket = SocketConnection();
  final DevToolsStore<AppState> store;

  _ChatBotView(this.store);

  @override
  void initState() {
    super.initState();
    socket = SocketConnection();
    socket.connection();
    socket.on('message', collectMessage);

    addMessage(
        "Hey there! I'm Lot Chatbot. I can help you find your next flight destination.",
        true,
        false,
        false,
        null);

    if (store.state.query.sentence.length > 0) {
      _handleSubmitted(store.state.query.sentence);
      store.dispatch(SetQueryAction(Query('')));
    }
  }

  @override
  void dispose() {
    socket.disconnect();
    super.dispose();
  }

  void collectMessage(type, status, text) {
    if (json.decode(text) == null) {
      addMessage(
          "I am really sorry. I haven't found any suitable solution.",
          true,
          false,
          false,
          null);
    } else if (type == 'weather') {
      addMessage('', true, true, false, json.decode(text));
    } else if (type == 'flights') {
      addMessage('', true, false, true, json.decode(text));
    } else {
      addMessage(text, true, false, false, null);
    }
  }

  final List<ChatMessage> _messages = <ChatMessage>[];
  final TextEditingController _textController = TextEditingController();

  void _handleSubmitted(String text) {
    _textController.clear();
    addMessage(text, false, false, false, null);
    socket.emit('message', text);
  }

  void addMessage(
      String text, bool isBot, bool isWeather, bool isTrip, var data) {
    ChatMessage message = ChatMessage(
        text: text,
        isBot: isBot,
        isWeather: isWeather,
        isTrip: isTrip,
        data: data);
    setState(() {
      _messages.insert(0, message);
    });
  }

  Widget _buildTextComposer() {
    return IconTheme(
      data: IconThemeData(color: Theme.of(context).accentColor),
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Row(
          children: <Widget>[
            Flexible(
              child: TextField(
                autofocus: true,
                controller: _textController,
                onSubmitted: _handleSubmitted,
                decoration:
                    InputDecoration.collapsed(hintText: "Send a message"),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 4.0),
              child: IconButton(
                  icon: Icon(Icons.send),
                  onPressed: () => _handleSubmitted(_textController.text)),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Flexible(
          child: ListView.builder(
            padding: EdgeInsets.all(8.0),
            reverse: true,
            itemBuilder: (_, int index) => _messages[index],
            itemCount: _messages.length,
          ),
        ),
        Divider(height: 1.0),
        Container(
          decoration: BoxDecoration(color: Theme.of(context).cardColor),
          child: _buildTextComposer(),
        ),
      ],
    );
  }
}

class ChatMessage extends StatelessWidget {
  ChatMessage({this.text, this.isBot, this.isWeather, this.isTrip, this.data});

  final String text;
  final bool isBot;
  final bool isWeather;
  final bool isTrip;
  var data;

  Widget weatherWidget(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(top: 0.0),
                child: Text('Weather in ' + data["city"] + ": ",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                    )),
              )
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                child: Image.network("http://openweathermap.org/img/wn/" +
                    data["icon"] +
                    ".png"),
              ),
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      child: Text(
                        data["city"] + " " + data["temp"].toString() + " °C",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 15.0,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                    Container(
                      child: Text(
                        data["description"],
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 15.0,
                            fontWeight: FontWeight.w100,
                            fontStyle: FontStyle.italic),
                      ),
                    )
                  ],
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  Widget tripWidget(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(top: 0.0),
                child: Text('Trip to ' + data["city"] + ": ",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                    )),
              )
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      child: Text(
                        data["price"].toString(),
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 15.0,
                            fontWeight: FontWeight.w100,
                            fontStyle: FontStyle.italic),
                      ),
                    ),
                    RaisedButton(
                      child: const Text('BOOK'),
                      onPressed: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  WebViewScreen(data['url']))),
                    )
                  ],
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        child: Container(
          color:
              !isBot ? Color.fromRGBO(241, 241, 241, 1) : LotPallete.mainBlue,
          padding: EdgeInsets.all(10.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                alignment: Alignment(0.0, 0.0),
                margin: const EdgeInsets.only(right: 16.0),
                child: CircleAvatar(
                  child: isBot
                      ? Image.asset("assets/profile.png", fit: BoxFit.fill)
                      : Text(
                          "Y",
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                  backgroundColor: !isBot ? LotPallete.mainBlue : Colors.white,
                ),
              ),
              Flexible(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    !isBot
                        ? Text("You",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: !isBot ? Colors.black : Colors.white))
                        : Container(),
                    Container(
                      margin: const EdgeInsets.only(top: 0.0),
                      child: isWeather
                          ? weatherWidget(context)
                          : isTrip
                              ? tripWidget(context)
                              : Text(
                                  text,
                                  overflow: TextOverflow.visible,
                                  style: TextStyle(
                                      color:
                                          !isBot ? Colors.black : Colors.white),
                                ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
        padding: EdgeInsets.all(10.0),
      ),
    );
  }
}

BoxDecoration messageDecoration() {
  return BoxDecoration(
      border: Border.all(
        width: 1,
      ),
      borderRadius: BorderRadius.all(Radius.circular(10.0)));
}
