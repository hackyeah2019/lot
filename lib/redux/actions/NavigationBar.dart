import 'package:Lot/redux/models/NavigationBarIndex.dart';

class GetNavigationBarIndex {
  GetNavigationBarIndex();
}

class SetNavigationBarIndex {
  final int index;

  SetNavigationBarIndex(this.index);
}
