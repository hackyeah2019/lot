import 'package:Lot/redux/models/User.dart';

class GetUserAction {
  GetUserAction();
}

class SignOutUser {
  SignOutUser();
}

class LogInUser {
  final User user;

  LogInUser(this.user);
}
