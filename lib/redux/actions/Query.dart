import 'package:Lot/redux/models/Query.dart';

class GetQueryAction {
  GetQueryAction();
}

class SetQueryAction {
  final Query query;

  SetQueryAction(this.query);
}
