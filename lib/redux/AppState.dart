import 'package:Lot/redux/models/NavigationBarIndex.dart';
import 'package:Lot/redux/models/Query.dart';
import 'package:Lot/redux/models/User.dart';

class AppState {
  static var empty =
      AppState(new User('', false), new NavigationBarIndex(0), new Query(''));

  final User user;
  final NavigationBarIndex index;
  final Query query;

  AppState(this.user, this.index, this.query);
}
