import 'package:Lot/redux/AppState.dart';
import 'package:Lot/redux/actions/NavigationBar.dart';
import 'package:Lot/redux/models/NavigationBarIndex.dart';
import 'package:Lot/redux/models/Query.dart';
import 'package:Lot/redux/models/User.dart';

AppState setIndex(User user, NavigationBarIndex index, Query query,
    SetNavigationBarIndex action) {
  index.index = action.index;

  return AppState(user, index, query);
}

AppState getIndex(User user, NavigationBarIndex index, Query query,
    GetNavigationBarIndex action) {
  return AppState(user, index, query);
}
