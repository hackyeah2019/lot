import 'package:Lot/redux/AppState.dart';
import 'package:Lot/redux/actions/NavigationBar.dart';
import 'package:Lot/redux/actions/Query.dart';
import 'package:Lot/redux/reducers/NavigationBarIndex.dart';
import 'package:Lot/redux/actions/User.dart';
import 'package:Lot/redux/reducers/Query.dart';
import 'package:Lot/redux/reducers/User.dart';

AppState appStateReducers(AppState state, dynamic action) {
  if (action is GetUserAction) {
    return getUser(state.user, state.index, state.query, action);
  } else if (action is LogInUser) {
    return setUser(state.user, state.index, state.query, action);
  } else if (action is SignOutUser) {
    return signOutUser(state.user, state.index, state.query, action);
  }
  if (action is GetNavigationBarIndex) {
    return getIndex(state.user, state.index, state.query, action);
  } else if (action is SetNavigationBarIndex) {
    return setIndex(state.user, state.index, state.query, action);
  }

  if (action is SetQueryAction) {
    return setSentence(state.user, state.index, state.query, action);
  } else if (action is GetQueryAction) {
    return getSentence(state.user, state.index, state.query, action);
  }
  return state;
}
