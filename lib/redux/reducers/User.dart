import 'package:Lot/redux/AppState.dart';
import 'package:Lot/redux/models/NavigationBarIndex.dart';
import 'package:Lot/redux/models/Query.dart';
import 'package:Lot/redux/models/User.dart';
import 'package:Lot/redux/actions/User.dart';

AppState setUser(
    User user, NavigationBarIndex index, Query query, LogInUser action) {
  user.email = action.user.email;
  user.logged = action.user.logged;

  return AppState(user, index, query);
}

AppState signOutUser(
    User user, NavigationBarIndex index, Query query, SignOutUser action) {
  user.email = null;
  user.logged = false;
  return AppState(user, index, query);
}

AppState getUser(
    User user, NavigationBarIndex index, Query query, GetUserAction action) {
  return AppState(user, index, query);
}
