import 'package:Lot/redux/AppState.dart';
import 'package:Lot/redux/actions/Query.dart';
import 'package:Lot/redux/models/NavigationBarIndex.dart';
import 'package:Lot/redux/models/Query.dart';
import 'package:Lot/redux/models/User.dart';

AppState setSentence(
    User user, NavigationBarIndex index, Query query, SetQueryAction action) {
  query.sentence = action.query.sentence;

  return AppState(user, index, query);
}

AppState getSentence(
    User user, NavigationBarIndex index, Query query, GetQueryAction action) {
  return AppState(user, index, query);
}
