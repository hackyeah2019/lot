import 'package:flutter/material.dart';
import 'dart:math';

class Recommendation {
  String country;
  String city;
  String iata;
  Image image;

  Recommendation({this.country, this.city, this.iata, this.image});

  factory Recommendation.fromJson(Map<String, dynamic> json) {
    return Recommendation(
      country: json['country'],
      city: json['cities'],
      iata: json['title'],
      image: json['body'],
    );
  }
}

String getRandomPlaceholderPath() {
  List<String> paths = ['assets/placeholder1.jpg','assets/placeholder2.jpg','assets/placeholder3.jpg'];
  var rng = new Random();
  return paths[rng.nextInt(3)];
}

class RecommendedCountry {
  String country;
  String city;
  String iata;
  Image image;

  RecommendedCountry({this.country, this.city, this.iata, this.image});

  factory RecommendedCountry.fromJson(Map<String, dynamic> json) {
    return RecommendedCountry(
      country: json['country'],
      city: json['city'],
      iata: json['iata'],
      image: Image.asset(getRandomPlaceholderPath(), fit: BoxFit.fill),
    );
  }
}

