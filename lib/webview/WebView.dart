import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:flutter/material.dart';

class WebViewScreen extends StatelessWidget {
  WebViewScreen(this.webViewUrl);
  final String webViewUrl;


  @override
  Widget build(BuildContext context) {
    return new WebviewScaffold(
        url: webViewUrl,
        appBar: new AppBar(
          title: new Image.asset('assets/logo.png', fit: BoxFit.cover, height: 32),
        ),
      );
  }
}