import 'package:Lot/redux/AppState.dart';
import 'package:Lot/redux/actions/User.dart';

import 'package:Lot/utils/LotPallete.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter/material.dart';

typedef SignOutUserCallback = Function();

class Settings extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, SignOutUserCallback>(
      converter: (store) {
        return () => store.dispatch(
              SignOutUser(),
            );
      },
      builder: (context, callback) {
        return SettingsView(callback);
      },
    );
  }
}

class SettingsView extends StatelessWidget {
  final SignOutUserCallback callback;
  SettingsView(this.callback);

  void _signOut() {
    callback();
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Container(
              width: MediaQuery.of(context).size.width,
              child: RaisedButton(
                  onPressed: () => _signOut(),
                  padding: EdgeInsets.all(10.0),
                  color: LotPallete.mainBlue,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(right: 10.0),
                        child: Icon(
                          Icons.power_settings_new,
                          color: Colors.white,
                        ),
                      ),
                      Text('Sign Out',
                          style: TextStyle(
                              fontSize: 20,
                              color: Colors.white,
                              fontWeight: FontWeight.w100))
                    ],
                  )))
        ],
      ),
    );
  }
}
