import 'package:flutter/material.dart';

class Weather extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Card(
        child: Column(children: <Widget>[
      Container(
        padding: EdgeInsets.symmetric(vertical: 40.0),
        child: Image.asset('assets/logo.png', fit: BoxFit.cover, height: 100),
      ),
      Container(
          padding: EdgeInsets.symmetric(vertical: 20.0), child: Text('40℃')),
      Container(
          padding: EdgeInsets.symmetric(vertical: 20.0), child: Text('Sunny')),
    ]));
  }
}
