import 'dart:convert';

import 'package:Lot/discover/components/ListItem.dart';
import 'package:Lot/redux/models/Recommendation.dart';
import 'package:Lot/utils/LotPallete.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class Discover extends StatelessWidget {
  Future<List<RecommendedCountry>> fetchRecommendations() async {
    final response =
        await http.get('https://lkononov.herokuapp.com/lot/airports');

    if (response.statusCode == 200) {
      List responseJson = json.decode(response.body);
      return responseJson
          .map((m) => new RecommendedCountry.fromJson(m))
          .toList();
    } else {
      // If that response was not OK, throw an error.
      throw Exception('Failed to load recommedations');
    }
  }

  @override
  Widget build(BuildContext context) {
    return new FutureBuilder<List<RecommendedCountry>>(
      future: fetchRecommendations(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          List<RecommendedCountry> countries = snapshot.data;
          return new Container(
              color: LotPallete.backgroundGray,
              child: ListView.separated(
                padding: const EdgeInsets.all(8),
                itemCount: countries.length,
                itemBuilder: (BuildContext context, int index) {
                  return ListItemElement(countries[index]);
                },
                separatorBuilder: (BuildContext context, int index) =>
                    const Divider(),
              ));
        } else if (snapshot.hasError) {
          return Text("${snapshot.error}");
        }

        // By default, show a loading spinner.
        return Center(child: CircularProgressIndicator());
      },
    );

//
  }
}
