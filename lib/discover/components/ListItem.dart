import 'package:Lot/redux/actions/NavigationBar.dart';
import 'package:Lot/redux/actions/Query.dart';
import 'package:Lot/redux/models/Query.dart';
import 'package:Lot/redux/models/Recommendation.dart';
import 'package:flutter/material.dart';
import 'package:Lot/redux/AppState.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux_dev_tools/redux_dev_tools.dart';
import 'package:intl/intl.dart';
import 'dart:async';

typedef QueryCallback = Function(String);

class ListItemElement extends StatelessWidget {
  final RecommendedCountry country;

  ListItemElement(this.country);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, QueryCallback>(
      converter: (store) {
        return (query) => store.dispatch(
              SetQueryAction(Query(query)),
            );
      },
      builder: (context, callback) {
        return StoreBuilder<AppState>(
          builder: (context, store) => ListItem(country, callback, store),
        );
      },
    );
  }
}

class ListItem extends StatefulWidget {
  final RecommendedCountry country;
  final QueryCallback callback;
  final DevToolsStore<AppState> store;

  ListItem(this.country, this.callback, this.store);

  @override
  _ListItemState createState() => _ListItemState(country, callback, store);
}

class _ListItemState extends State<ListItem> {
  final RecommendedCountry country;
  final QueryCallback callback;
  final DevToolsStore<AppState> store;

  _ListItemState(this.country, this.callback, this.store);

  DateTime departureDate = DateTime.now();
  var formatter = new DateFormat.yMMMMd("en_US");

  void _doQuery(String query) {
    callback(query);
    store.dispatch(
      SetNavigationBarIndex(1),
    );
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: departureDate,
        firstDate: DateTime(DateTime.now().year, DateTime.now().month),
        lastDate: DateTime(2101));
    if (picked != null && picked != departureDate)
      setState(() {
        departureDate = picked;
      });
    _doQuery(
        "Can I fly to ${this.country.city} on ${formatter.format(departureDate)}?");
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            country.image,
            ListTile(
              title: Text(country.city),
              subtitle: Text(country.country),
            ),
            ButtonTheme.bar(
              // make buttons use the appropriate styles for cards
              child: ButtonBar(
                children: <Widget>[
                  FlatButton(
                      child: const Text('AVAILABILITY'),
                      onPressed: () => _selectDate(context)),
                  FlatButton(
                    child: const Text('WEATHER'),
                    onPressed: () {
                      _doQuery("What is the weather in ${this.country.city}?");
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
