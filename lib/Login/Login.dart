import 'package:Lot/redux/actions/NavigationBar.dart';
import 'package:flutter/material.dart';
import 'package:redux_dev_tools/redux_dev_tools.dart';
import 'package:Lot/redux/AppState.dart';
import 'package:Lot/Login/helpers/Triangle.dart';
import 'package:Lot/redux/models/User.dart';
import 'package:Lot/redux/actions/User.dart';

class LoginScreen extends StatelessWidget {
  final DevToolsStore<AppState> store;

  LoginScreen(this.store);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(title: 'Login Form', home: Login(this.store));
  }
}

class Login extends StatefulWidget {
  final DevToolsStore<AppState> store;

  Login(this.store);

  @override
  State createState() => LoginForm(this.store);
}

class LoginForm extends State<Login> {
  final DevToolsStore<AppState> store;
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  bool isValidationError = false;

  LoginForm(this.store);

  @override
  void initState() {
    super.initState();

    store.dispatch(SetNavigationBarIndex(0));
  }

  void _submitLoginForm() {
    if (emailController.text.trim() != '' &&
        passwordController.text.trim() != '') {
      emailController.clear();
      passwordController.clear();
      store.dispatch(LogInUser(User(emailController.text, true)));
    } else {
      isValidationError = true;
    }
  }

  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        body: Container(
            color: Color.fromRGBO(46, 82, 141, 1),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.symmetric(vertical: 40.0),
                  child: Image.asset('assets/logo.png',
                      fit: BoxFit.cover, height: 100),
                ),
                Container(
                    padding:
                        EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
                    child: Text(
                      'Log in',
                      style: TextStyle(
                          fontSize: 25,
                          color: Colors.white,
                          letterSpacing: 1.1,
                          fontWeight: FontWeight.w500),
                    )),
                Container(
                  padding:
                      EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
                  child: TextField(
                    controller: emailController,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                    ),
                    decoration: InputDecoration(
                        labelText: 'Email',
                        contentPadding: EdgeInsets.symmetric(vertical: 5),
                        focusedBorder: new UnderlineInputBorder(
                            borderSide: new BorderSide(color: Colors.white)),
                        enabledBorder: new UnderlineInputBorder(
                            borderSide: new BorderSide(color: Colors.white)),
                        labelStyle:
                            TextStyle(color: Colors.white, fontSize: 13)),
                  ),
                ),
                Container(
                  padding:
                      EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                  child: TextField(
                    controller: passwordController,
                    obscureText: true,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                    ),
                    decoration: InputDecoration(
                        labelText: 'Password',
                        contentPadding: EdgeInsets.symmetric(vertical: 5),
                        focusedBorder: new UnderlineInputBorder(
                            borderSide: new BorderSide(color: Colors.white)),
                        enabledBorder: new UnderlineInputBorder(
                            borderSide: new BorderSide(color: Colors.white)),
                        labelStyle:
                            TextStyle(color: Colors.white, fontSize: 13)),
                  ),
                ),
                Container(
                  alignment: Alignment.bottomRight,
                  padding:
                      EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                  child: Text(
                    'Forgot Password?',
                    style: TextStyle(
                      color: Colors.white,
                      letterSpacing: 1.2,
                      fontSize: 10.0,
                    ),
                  ),
                ),
                Container(
                    margin: EdgeInsets.only(top: 20.0, bottom: 100.0),
                    padding: EdgeInsets.symmetric(horizontal: 20.0),
                    child: OutlineButton(
                      child: Text(
                        'Next',
                        style: TextStyle(
                            color: Colors.white,
                            letterSpacing: 1.3,
                            fontWeight: FontWeight.w100),
                      ),
                      onPressed: () => _submitLoginForm(),
                      borderSide: BorderSide(
                        color: Colors.white,
                        style: BorderStyle.solid,
                        width: 0.8,
                      ),
                    )),
                Row(
                  children: <Widget>[
                    ClipPath(
                      child: Container(
                        color: Colors.white,
                        alignment: Alignment.bottomLeft,
                        padding:
                            EdgeInsets.symmetric(vertical: 30, horizontal: 30),
                        child: Text(
                          'Terms & Conditions',
                          style: TextStyle(
                              color: Colors.black, fontWeight: FontWeight.w100),
                        ),
                        height: 200.0,
                        width: MediaQuery.of(context).size.width,
                      ),
                      clipper: TriangleClipper(),
                    )
                  ],
                )
              ],
            )));
  }
}
