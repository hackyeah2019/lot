import 'package:Lot/redux/actions/NavigationBar.dart';
import 'package:Lot/utils/LotPallete.dart';
import 'package:flutter/material.dart';
import 'package:Lot/redux/AppState.dart';
import 'package:Lot/Settings/Settings.dart';
import 'package:Lot/Discover/Discover.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux_dev_tools/redux_dev_tools.dart';
// import 'package:flutter_redux_dev_tools/flutter_redux_dev_tools.dart';
import 'package:Lot/chatbot/Chatbot.dart';

typedef SetSelectedTabIndexCallback = Function(int);

class HomePageView extends StatelessWidget {
  final DevToolsStore<AppState> store;
  HomePageView(this.store);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, SetSelectedTabIndexCallback>(
      converter: (store) {
        return (index) => store.dispatch(
              SetNavigationBarIndex(index),
            );
      },
      builder: (context, callback) {
        return HomePage(store, callback);
      },
    );
  }
}

class HomePage extends StatefulWidget {
  final DevToolsStore<AppState> store;
  final SetSelectedTabIndexCallback callback;

  HomePage(this.store, this.callback);
  @override
  State createState() => _HomePageState(store, callback);
}

class _HomePageState extends State<HomePage> {
  final DevToolsStore<AppState> store;
  final SetSelectedTabIndexCallback callback;

  _HomePageState(this.store, this.callback);

  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);

  static List<Widget> _widgetOptions = <Widget>[
    Discover(),
    ChatBot(),
    Settings(),
  ];

  void _onItemTapped(int index) {
    callback(index);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
          title: Image.asset('assets/logo.png', fit: BoxFit.cover, height: 32)),
      body: _widgetOptions.elementAt(store.state.index.index),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            title: Text('Discover'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.chat),
            title: Text('Chat'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            title: Text('Settings'),
          ),
        ],
        currentIndex: store.state.index.index,
        selectedItemColor: LotPallete.mainBlue,
        onTap: _onItemTapped,
      ),
      /* endDrawer: new Container(
          width: 240.0, color: Colors.white, child: new ReduxDevTools(store)), */
    );
  }
}
